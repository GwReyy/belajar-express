const { response } = require('express')
const express = require ('express')
const userRouter = express.Router()
const userController = require ('../controller/userController')

//.route untuk menggabungkan jika memiliki path yg sama
userRouter.route('/user')
    .get(userController.index)

    .post(userController.add)

userRouter.put('/user/:id', userController.update)

userRouter.delete('/user/:id', userController.delete)

module.exports = userRouter