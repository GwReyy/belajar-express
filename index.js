//Basic Routing
const express = require('express')
const userRouter = require('./routing/routing')
const app = express()

app.use(express.json())//parsing menggunakan data json
app.use(express.urlencoded({extended: true})) //for parsing application /x-www-urlencoded

app.get('/', (req,res)=>{
    res.send('Home')
})

app.use(userRouter)

app.listen(3000, function(){
    console.log('Server Working')
})